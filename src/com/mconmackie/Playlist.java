package com.mconmackie;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class Playlist {
    private ArrayList<Album> albums;
    private LinkedList<Song> playList;

    public Playlist() {
        this.albums = new ArrayList<Album>();
        this.playList = new LinkedList<Song>();
    }

    public ArrayList<Album> getAlbums() {
        return this.albums;
    }

    public LinkedList<Song> getPlayList() {
        return this.playList;
    }

    public void addAlbum(String title) {
        if(findAlbum(title) == null) {
            Album newAlbum = new Album(title);
            albums.add(newAlbum);
            System.out.println("Album added to collection");
            return;
        }
        System.out.println("Album already exists in collection");
        return;
    }

    public void removeAlbum(String title) {
        Album album = findAlbum(title);
        if(album == null) {
            System.out.println("Album not in collection");
            return;
        }
        albums.remove(album);
        System.out.println("Album removed from collection");
    }

    public Album findAlbum(String title) {
        if (!albums.isEmpty()) {
            for (int i = 0; i < this.albums.size(); i++) {
                Album album = this.albums.get(i);
                if (album.getTitle().equalsIgnoreCase(title))
                    return album;
            }
        }
        return null;
    }

    public void printAllAlbums(boolean includeSongs) {
        for(int i = 0; i < this.albums.size(); i++)
            printAlbum(this.albums.get(i), includeSongs);
    }

    public void printAlbum(String albumTitle, boolean includeSongs) {
        Album album = findAlbum(albumTitle);
        if(album != null) {
            printAlbum(album, includeSongs);
            return;
        }
        System.out.println("Album not in collection");
    }

    public void printAlbum(Album album, boolean includeSongs) {
        System.out.println("Album [" + (albums.indexOf(album) + 1) + "] Title: \"" + album.getTitle() + "\"");
        if (includeSongs) {
            if (!album.printSongs())
                System.out.println("Album has no songs");
        }
    }

    public void addAlbumSong(String albumTitle, String songTitle, String duration) {
        Album album = findAlbum(albumTitle);
        if(album != null) {
            album.addSong(songTitle, duration);
            return;
        }
        System.out.println("Album not in collection - song not added");
        return;
    }

    public void removeAlbumSong(String albumTitle, String songTitle) {
        Album album = findAlbum(albumTitle);
        if(album == null) {
            System.out.println("Album not in collection - song not removed");
            return;
        }
        Song song = album.findSong(songTitle);
        if(song == null) {
            System.out.println("Song not in album - not removed");
            return;
        }
        if(album.removeSong(song))
            System.out.println("Song removed from album");
        else
            System.out.println("Oops, somethig went wrong while removing song - not removed");

    }

    public void addPlaylistSong(String albumTitle, String songTitle) {
        Album album = findAlbum(albumTitle);
        if(album == null) {
            System.out.println("Album not in collection - song not added to play list");
            return;
        }
        Song song = album.findSong(songTitle);
        if(song == null) {
            System.out.println("Can't find song in album - not added to play list");
        } else {
            if(playList.add(song))
                System.out.println("Song added to playlist");
            else
                System.out.println("Oops, something went wrong while adding song to play list");
        }
    }

    public void print() {
        if(playList.isEmpty()) {
            System.out.println("Playlist is empty");
            return;
        }
        ListIterator<Song> playlistIterator = playList.listIterator();
        while(playlistIterator.hasNext()) {
            Song song = playlistIterator.next();
            System.out.println("Playlist [" + (playList.indexOf(song)+1) + "]  Title: " + song.getTitle() + "  Duration: " + song.getDuration());
        } 
    }

}