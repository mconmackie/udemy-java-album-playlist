package com.mconmackie;

import java.util.Scanner;

public class Driver {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Playlist playlist = new Playlist();
        boolean quit = false;
        String answer;
        String albumTitle;
        String songTitle;
        String duration;
        boolean includeSongs;
        while (!quit) {
            System.out.print("Enter action: ");
            String action = scanner.nextLine().toUpperCase();
            switch (action) {
                case "?":
                case "HELP":
                    System.out.println("Valid actions are ...\n" + 
                        "ADDALBUM\tAdd a new album to the collection\n" +
                        "REMOVEALBUM\tRemove entire album from the collection\n" +
                        "ADDSONG\t\tAdd a song to the specified album\n" + 
                        "REMOVESONG\tRemove a song from an album\n" +
                        "ADDPLAYLIST\tAdd a song from the album colletion to the paly list\n" +
                        "REMOVEPLAYLIST\tRemove a song from the play list\n" +
                        "PRINT\t\tPrints the requested album with or without songs\n" +
                        "PRINTALL\tPrint all albums with or without songs\n" + 
                        "PRINTPLAYLIST\tPrint all songs in the playlist\n" +
                        "?/HELP\t\tThis display\n" +
                        "QUIT\t\tIf I have to explain ...");
                    break;

                case "QUIT":
                    quit = true;
                    break;

                case "ADDALBUM":
                    System.out.println("Enter new album title:");
                    albumTitle = scanner.nextLine();
                    playlist.addAlbum(albumTitle);
                    break;

                case "REMOVEALBUM":
                    System.out.println("Enter album title to remove:");
                    albumTitle = scanner.nextLine();
                    playlist.removeAlbum(albumTitle);
                    break;

                case "ADDSONG":
                    System.out.println("Enter album title:");
                    albumTitle = scanner.nextLine();
                    if (playlist.findAlbum(albumTitle) != null) {
                        System.out.println("Enter song title:");
                        songTitle = scanner.nextLine();
                        System.out.println("Enter song duration:");
                        duration = scanner.nextLine();
                        playlist.addAlbumSong(albumTitle, songTitle, duration);
                    } else {
                        System.out.println("Album not in colletion - cannot add song");
                    }
                    break;

                case "REMOVESONG":
                    System.out.println("Enter album title:");
                    albumTitle = scanner.nextLine();
                    System.out.println("Enter song to remove:");
                    songTitle = scanner.nextLine();
                    playlist.removeAlbumSong(albumTitle, songTitle);
                    break;

                case "ADDPLAYLIST":
                    System.out.println("Enter album title containing song:");
                    albumTitle = scanner.nextLine();
                    System.out.println("Enter song title to add to play list:");
                    songTitle = scanner.nextLine();
                    playlist.addPlaylistSong(albumTitle, songTitle);
                    break;

                case "PRINT":
                    System.out.println("Enter album title:");
                    albumTitle = scanner.nextLine();
                    System.out.println("Print songs with the album [y|N]?");
                    answer = scanner.nextLine();
                    if(answer.isEmpty()) {
                        answer = "N";
                    }
                    includeSongs = false;
                    if(answer.substring(0, 1).equalsIgnoreCase("Y"))
                        includeSongs = true;
                    playlist.printAlbum(albumTitle, includeSongs);
                    break;

                case "PRINTALL":
                    System.out.println("Do you want to print songs with the albums [y/N]?");
                    answer = scanner.nextLine().toUpperCase();
                    if(answer.isEmpty())
                        answer = "N";
                    includeSongs = false;
                    if(answer.substring(0, 1).equalsIgnoreCase("Y"))
                        includeSongs = true;
                    playlist.printAllAlbums(includeSongs);
                    break;

                case "PRINTPLAYLIST":
                    playlist.print();
                    break;

                default:
                    System.out.println("\"" + action + "\" is not recognized");
            }
        }
        // String newAlbumTitle = "The Hobbit - An Unexpected Journey";
        // Album newAlbum = playlist.addAlbum(newAlbumTitle);
        // if(newAlbum == null)
        //     System.out.println("Sorry, album title " + newAlbumTitle + " already exists");
        // newAlbum.addSong("My Dear Frodo", "1:03");
        // newAlbum.addSong("Old Friends", "3:12");
        // newAlbum.addSong("An Unexpected Party", "2:44");
        // newAlbum.addSong("Axe or Sword", "1:57");
        // newAlbum.addSong("Misty Mountains (performed by Richard Armitage and The Dwarf Cast)", "4:37");
        // newAlbum.addSong("The Adventure Begine", "2:10");
        // newAlbum.addSong("The World is Ahead", "1:48");
        // newAlbum.addSong("An Ancient Enemy", "2:15");
        // newAlbum.addSong("Radagast the Brown", "2:27");
        // newAlbum.addSong("Roast Mutton", "3:05");
        // newAlbum.addSong("A Troll-hoard", "2:51");
        // newAlbum.addSong("The Hill of Sorcery", "1:41");
        // newAlbum.addSong("Warg-scouts", "3:11");
        // playlist.printAlbums(false);
        // playlist.printAlbums(true);
    }
}