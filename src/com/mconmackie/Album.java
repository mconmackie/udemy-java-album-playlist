package com.mconmackie;
import java.util.LinkedList;
import java.util.ListIterator;

public class Album {
   private String title;
   private LinkedList<Song> songs;
   
   public Album(String title) {
       this.title = title;
       this.songs = new LinkedList<Song>();
   }

   public String getTitle() {
       return this.title;
   }

   public LinkedList<Song> getSongs() {
       return this.songs;
   }

   public void addSong(String title, String duration) {
       Song newSong = new Song(title, duration);
       if(!this.songs.contains(newSong)) {
           this.songs.add(newSong);
           System.out.println("Song added to album");
           return;
       }
       System.out.println("Song already in album - not added");
       return;
   }

   public boolean printSongs() {
       if (this.songs != null) {
           ListIterator<Song> songIterator = this.songs.listIterator();
           int i = 0;
           while (songIterator.hasNext()) {
               Song song = songIterator.next();
               System.out.println("Song [" + (++i) + "] Title: " + song.getTitle() + "   Duration: " + song.getDuration());
           }
           return true;
       }
       return false;
   }

   public Song findSong(String songTitle) {
       ListIterator<Song> songIterator = this.songs.listIterator();
       while(songIterator.hasNext()) {
           Song song = songIterator.next();
           if(song.getTitle().equalsIgnoreCase(songTitle))
                return song;
       }
       return null;
   }

   public boolean removeSong(Song song) {
        return this.songs.remove(song);
   }
}